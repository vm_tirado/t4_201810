package view;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import controller.Controller;
import model.vo.Taxi;
import model.vo.Compania;
import model.vo.InfoTaxiRango;
import model.vo.RangoFechaHora;
import model.vo.Servicio;

public class TaxiTripsManagerView 
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
		Date fechaI = new Date();
		Date fechaF = new Date();
		while(!fin)
		{
			printMenu();
			
			int option = sc.nextInt();
			
			switch(option)
			{
				case 1:
					
				
					//fecha inicial
					System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
					String fechaInicialReq1A = sc.next();

					//hora inicial
					System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
					String horaInicialReq1A = sc.next();

					
					try {
						fechaI = formato.parse(fechaInicialReq1A+'T'+horaInicialReq1A+":00.000");
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					//fecha final
					System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
					String fechaFinalReq1A = sc.next();

					//hora final
					System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
					String horaFinalReq1A = sc.next();
					
					try {
						fechaF = formato.parse(fechaFinalReq1A+'T'+horaFinalReq1A+":00.000");
					} catch (ParseException e) {
						
						
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					//VORangoFechaHora
					fechaI.setHours(fechaI.getHours()+1);
					fechaF.setMinutes(fechaF.getMinutes()+15);

					RangoFechaHora rangoReq1A = new RangoFechaHora(fechaI, fechaF);
					InfoTaxiRango[] respuesta = Controller.loadServices(rangoReq1A);
//					System.out.println(respuesta[2].getIdTaxi());
					
					break;
					
					
					
				case 2:
					int [] resultadoOrdenInv = Controller.servicesInInverseOrder();
					
					System.out.println("Total de servicios en orden inverso de tiempo de inicio "+ resultadoOrdenInv[0]);
					System.out.println("Total de servicios que No estan en orden inverso de tiempo de inicio "+ resultadoOrdenInv[1]);
										
					break;
				case 3:
					Compania [] resultadoOrden = Controller.servicesPriority();
					for (int i=1; i<resultadoOrden.length -1; i++)
					{
					System.out.println(resultadoOrden[i].getNombre()+":"
							+ " taxis "+ resultadoOrden[i].getTaxisInscritos().size()
							+ " Servicios " + resultadoOrden[i].getNumServicios());
					
					}
					break;
				case 4:	
					fin=true;
					sc.close();
					break;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 3----------------------");
		System.out.println("1. Cargar los servicios de un taxi reportados en un rango de fechas espec�fico");
		System.out.println("2. Verificar el ordenamiento inverso de los servicios del taxi por tiempo de inicio");
		System.out.println("3. Verificar el ordenamiento de los servicios de la compania por prioridad orientada a mayor");
		System.out.println("4. Salir");
		System.out.println("Digite el n�mero de opci�n para ejecutar la tarea, luego presione enter: (Ej., 1):");
		
	}
}
