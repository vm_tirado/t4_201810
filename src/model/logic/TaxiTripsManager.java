package model.logic;

import java.io.FileReader;
import java.util.Date;
import java.util.Iterator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import api.ITaxiTripsManager;
import model.data_structures.HeapSort;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.List;
import model.data_structures.Node;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.Taxi;
import model.vo.Compania;
import model.vo.InfoTaxiRango;
import model.vo.RangoFechaHora;
import model.vo.Servicio;

public class TaxiTripsManager implements ITaxiTripsManager {

	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";

	private InfoTaxiRango[] arreglo;
	private Queue<Taxi> colaTaxis; 
	private Queue<Compania> colaCompanias;
	private Compania[] arregloCompanias;

	boolean bool = false;

	public InfoTaxiRango[] loadServices(String serviceFile, RangoFechaHora rango) {

		colaTaxis = new Queue<Taxi>();
		

		long time=System.currentTimeMillis();

		if (!bool)
		{



			try
			{


				Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();
				Servicio[] item= gson.fromJson(new FileReader(serviceFile), Servicio[].class);



				for(int i=0; i<item.length;i++)
				{
					Taxi taxito = new Taxi(item[i].getCompany(),item[i].getTaxiid());

					if(!colaTaxis.contains(taxito))

					{
						colaTaxis.enqueue(taxito);
					}

				}

				for(Servicio otroServicio: item)
				{

					for(Taxi taxito : colaTaxis)

					{
						if(taxito.getTaxiId()!=null&&taxito.getTaxiId().equalsIgnoreCase(otroServicio.getTaxiid()))
						{

							taxito.add(otroServicio);
						}

					}

				}				
				for(Taxi taxito: colaTaxis)
				{

					if (taxito.getTaxiId()!=null)
						for(Servicio unServicio: taxito.getServiciosInscritos())
						{
							if(unServicio.isInRange(rango))
							{
								taxito.add1(unServicio);

							}
						}

				}





				arreglo = new InfoTaxiRango[colaTaxis.size()+1];
				arreglo[0]= null;

				int i=1;

				for (Taxi taxito: colaTaxis)
				{

					if(taxito.getTaxiId()!=null)
					{
						String id= taxito.getTaxiId();
						double plataGanada = taxito.darplataGanadaF();
						int serviciosPrestadosEnRango = taxito.getServiciosInscritosFecha().size();
						double distanciaTotalRecorrida = taxito.darDistanciaF();


						arreglo[i] = new InfoTaxiRango(id,plataGanada, serviciosPrestadosEnRango, distanciaTotalRecorrida);
						i++;

					}

				}
				HeapSort ordenado = new HeapSort();
				ordenado.sort(arreglo,true);


				for(int x=0; x<arreglo.length;x++)
				{
					if(arreglo[x]!=null)
					{
						System.out.println("Id: "+arreglo[x].getIdTaxi()+ " N�mero de servicios: "+arreglo[x].getServiciosPrestadosEnRango());
					}
				}
				



				bool=true;
				System.gc();
				return arreglo;

			}

			catch (Exception e)
			{
				e.printStackTrace();
				System.out.println("Hubo un error al cargar el archivo");
			}


			System.out.println("Inside loadServices with " + serviceFile);
			System.out.println("tiempo "+(System.currentTimeMillis()-time));
		}

		else

		{
			System.err.println("El archivo solamente puede ser cargado una vez en el programa");


		}

		System.gc();
		return arreglo;


	}

	@Override
	public int[] servicesInInverseOrder() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int[] servicesInOrder() {
		// TODO Auto-generated method stub
		return null;
	}



	public Compania[] servicesPriority()
	{
		colaCompanias= new Queue<Compania>();
		Compania independent= new Compania("Independent Owner");
		colaCompanias.enqueue(independent);
		for( Taxi taxito:colaTaxis)
		{
			if(taxito.getTaxiId()!=null && taxito.getCompany()!=null )
			{
				Compania comp= new Compania(taxito.getCompany());
				if(!colaCompanias.contains(comp))
				{
					colaCompanias.enqueue(comp);
					comp.add(taxito);
				}
				else 
				{
					colaCompanias.get(comp).add(taxito);
				}
			}
			else if (taxito.getTaxiId()!=null)
			{
				Compania actual=colaCompanias.get(independent);
				actual.add(taxito);
			}
		}
		
		
		
		arregloCompanias = new Compania[colaCompanias.size()+1];
		arregloCompanias[0]=null;
		int i=1;
		for (Compania comp: colaCompanias)
		{
			if (comp!=null)
			arregloCompanias[i]=comp;
			i++;
		}
		HeapSort<Compania> sort= new HeapSort();
	//sort.sort(arregloCompanias, false);
	sortCompania(arregloCompanias);
		return arregloCompanias;
	}
	
	
    public void sortCompania(Compania arr[])
    {
        int n = arr.length;
 
        // Construye inicialmente el �rbol.
        	 
	        for (int i = n / 2 ; i > 0; i--)
	            heapifyI(arr, n, i);
	 
	        // Extrae los elementos del �rbol.
	        for (int i=n-1; i>1; i--)
	        {
	            // Mueve la ra�z actual al final.
	        	Compania temp = arr[1];
	            arr[1] = arr[i];
	            arr[i] = temp;
	 
	            // Aplica recursividad.
	            heapifyI(arr, i, 1);
	        }
        	
        	
        }
    void heapifyI(Compania arr[], int n, int i)
    {
        int largest = i;  // Inicializa el mayor como la ra�z.
        int l = 2*i;  // left = 2*i
        int r = 2*i + 1;  // right = 2*i + 1
 
        // Si el hijo de la izquierda es mayor a la ra�z.
        if (l < n)
        {
      	if(arr[l].getNumServicios()-arr[largest].getNumServicios()<0)
            largest = l;	
        }
 
        // Si el hijo de la derecha es mayor a la ra�z.
        if (r < n && arr[r].getNumServicios()-arr[largest].getNumServicios()<0)
            largest = r;
 
        // Si el mayor no es la ra�z
        if (largest != i)
        {
        	Compania swap = arr[i];
            arr[i] = arr[largest];
            arr[largest] = swap;
 
            // Aplica recursividad.
            heapifyI(arr, n, largest);
        }
    }

}






