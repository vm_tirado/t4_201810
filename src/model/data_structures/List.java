package model.data_structures;

import java.util.Iterator;

public class List <T extends Comparable <T>> implements LinkedList<T>, Iterable<T> {

	protected Node<T> first;
	protected Node<T> last;
	protected int size;

	public List()
	{
		first= new Node<T>(null, null, null);
		last=first;
		size=0;
	}

	@Override
	public void add(T obj) {
		if(first.getObject()!=null)
		{
			Node<T> n= new Node<T>(obj,null,last);
			last.changeNext(n);
			last=last.next();
			size++;
		}
		else
		{
			first.addObject(obj);
			size++;
		}	

	}

	@Override
	public T get(T obj) {
		T object=null;
		Node<T> curr=first;
		while(curr!=null && object==null)
		{
			if (curr.getObject().compareTo(obj)==0)
			{
				object=curr.getObject();
			}
			curr=curr.next();
		}
		return object;
	}

	@Override
	public int size() {

		return size;
	}

	@Override
	public T get(int position) {
		Node<T> curr=first;
		int j=0;
		while(curr!=null && j!= position)
		{
			curr=curr.next();
		}
		return curr.getObject();

	}

	@Override
	public void lisiting() {

		last=first;

	}
	
	public  Node<T> getFirst()
	{
		return first;
	}

	public Node<T> getLast() {
		return last;
	}

	@Override
	public Node<T> next() {
		Node<T> node=null;
		if(last.next().getObject()!=null)
		{
			last=last.next();
			node=last;
		}
		return node;
	}

	@Override
	public Iterator<T> iterator() {

		return new ListIterator();
	}
	private class ListIterator implements Iterator<T>
	{
		private Node<T> curr= first;
		@Override
		public boolean hasNext() {
			return curr!=null;
		}

		@Override
		public T next() {
			T next=curr.getObject();
			curr=curr.next();
			return next;
			
		}
	}
}
