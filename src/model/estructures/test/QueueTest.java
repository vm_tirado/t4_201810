package model.estructures.test;

import java.util.Iterator;

import junit.framework.TestCase;
import model.data_structures.LinkedList;
import model.data_structures.Node;
import model.data_structures.Queue;

public class QueueTest extends TestCase {



	private Queue cola;

	private int numeroElementos;


	/**
	 * Construye una nueva lista vacia de Integers
	 */
	public void setupEscenario1( )
	{
		cola = new Queue( );
		numeroElementos = 100;

	}

	/**
	 * Construye una nueva lista con 500 elementos inicialados con el valor de la posicion por 2
	 */
	public void setupEscenario2( )
	{
		numeroElementos = 10;
		cola = new Queue( );


		for( int cont = 0; cont < numeroElementos; cont++ )
		{
			cola.enqueue(( new Integer( cont* 2 ) ));
		}
		

	}

	public void setupEscenario3()
	{
		cola= new Queue();
	}



	/**
	 * Prueba que los elementos se est�n ingresando correctamente a partir de la cabeza
	 */
	public void testA�adir( )
	{
		setupEscenario1( );

		for( int cont = 0; cont < numeroElementos; cont++ )
		{
			cola.enqueue( new Integer( 5 * cont ) );

		}
		// Verificar que la lista tenga la longitud correcta
		assertEquals( "No se adicionaron todos los elementos", numeroElementos, cola.size( ) );


	}
	
	public void testA�adirPrimero()
	{
		setupEscenario3( );
		cola.enqueue( new Integer( 5 ) );
		int  nod=(Integer) cola.getFirst().getObject();
		assertEquals( "No se adicciona el primero", 5, cola.getFirst().getObject() );
		
		
	}



	/**
	 * Prueba que se retorne correctamente el primer elemento de la lista
	 */
	public void testDarPrimero( )
	{
		setupEscenario2( );

		Node nodo = cola.getFirst();

		Integer elemento = ( Integer )nodo.getObject();

		// Verificar que el primero elemento sea el correcto
		assertEquals( "El primer elemento no es correcto", 0, elemento.intValue( ) );

	}
	
	public void testUltimo()
	{
		setupEscenario2( );
		
		Node nodo= cola.getLast();
		Integer elemento = ( Integer )nodo.getObject();
		assertEquals( "El ultimo elemento no es correcto", 18, elemento.intValue( ) );
		
	}

	public void testEliminar()
	{
		setupEscenario3();
		cola.dequeue();

		assertEquals( "No debia de haber eliminado", 0, cola.size());
	}

	public void testEliminar2()
	{
		setupEscenario2();
		for(int i=0 ; i<numeroElementos; i++)
		{
		cola.dequeue();	
		}

		assertEquals( "No elimino todos los elementos", 0, cola.size());
	}

}
