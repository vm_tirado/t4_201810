package model.estructures.test;

import junit.framework.TestCase;
import model.data_structures.Node;
import model.data_structures.Stack;

public class StackTest extends TestCase{

	private Stack pila;

	private int numeroElementos;
	
	private int numeroArbitrario;


	/**
	 * Construye una nueva lista vacia de Integers
	 */
	public void setupEscenario1( )
	{
		numeroArbitrario=5;
		pila = new Stack( );
		numeroElementos = 100;
		
	}

	/**
	 * Construye una nueva lista con 500 elementos inicialados con el valor de la posicion por 2
	 */
	public void setupEscenario2( )
	{
		numeroElementos = 500;
		numeroArbitrario =2;
		
		pila = new Stack( );

		for( int cont = 0; cont < numeroElementos; cont++ )
		{
			pila.push(( new Integer( cont * numeroArbitrario ) ));
		}
		
	}
	
	public void setupEscenario3()
	{
		pila= new Stack();
		System.out.println(pila.size() + " add");
	}



	/**
	 * Prueba que los elementos se est�n ingresando correctamente a partir de la cabeza
	 */
	public void testA�adir( )
	{
		setupEscenario1( );


		for( int cont = 0; cont < numeroElementos; cont++ )
		{
			pila.push( new Integer( numeroArbitrario * cont ) );
			
		}

		// Verificar que la lista tenga la longitud correcta
		assertEquals( "No se adicionaron todos los elementos", numeroElementos, pila.size( ) );


	}
	
	public void testEliminar()
	{
		setupEscenario3( );

			pila.pop();
	assertEquals( "No debe eliminar nada", 0, pila.size( ) );
}


	public void testEliminar2()
	{
		setupEscenario2( );

		for(int i=0; i <numeroElementos; i++)
		{
			pila.pop();
			System.out.println(pila.size());
		}

	assertEquals( "No se eliminaron todos los elementos", 0, pila.size( ) );
}


	/**
	 * Prueba que se retorne correctamente el primer elemento de la lista (el mas reciente)
	 */
	public void testDarUltimo( )
	{
		setupEscenario2( );
		int numero = numeroElementos*numeroArbitrario -numeroArbitrario;

		Node nodo = pila.getlast();

		Integer elemento = ( Integer )nodo.getObject();

		// Verificar que el primero elemento sea el correcto
		assertEquals( "El último no es correcto", numero, elemento.intValue( ) );

	}
	

	}


