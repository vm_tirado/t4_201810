package model.vo;

import java.util.Date;

/**
 * Representation of a Service object
 */
public class Servicio implements Comparable<Servicio>
{	


	private String trip_id;
	private String taxi_id;
	private int trip_seconds;
	private double trip_miles;
	private double trip_total;
	private int dropoff_community_area;
	private Date trip_start_timestamp;
	private Date trip_end_timestamp;
	private String company;
	private int pickup_community_area;
	private double fare;


	
	
	public boolean isInRange(RangoFechaHora rango)
	{
		
		if(getDropoff_end_timestamp()!=null&&getDropoff_start_timestamp().before(rango.getFechaFinal())&&getDropoff_end_timestamp().after(rango.getFechaInicial()))
			
		{
			return true;
			
				
		}
		
		return false;
		
	}
	
	
	public void setTrip_id(String trip_id) {
		this.trip_id = trip_id;
	}


	public void setTaxi_id(String taxi_id) {
		this.taxi_id = taxi_id;
	}


	public void setTrip_seconds(int trip_seconds) {
		this.trip_seconds = trip_seconds;
	}


	public void setTrip_miles(double trip_miles) {
		this.trip_miles = trip_miles;
	}


	public void setTrip_total(double trip_total) {
		this.trip_total = trip_total;
	}


	public void setDropoff_community_area(int dropoff_community_area) {
		this.dropoff_community_area = dropoff_community_area;
	}


	public void setTrip_start_timestamp(Date trip_start_timestamp) {
		this.trip_start_timestamp = trip_start_timestamp;
	}


	public void setTrip_end_timestamp(Date trip_end_timestamp) {
		this.trip_end_timestamp = trip_end_timestamp;
	}


	public void setCompany(String company) {
		this.company = company;
	}


	public void setPickup_community_area(int pickup_community_area) {
		this.pickup_community_area = pickup_community_area;
	}


	public void setFare(double fare) {
		this.fare = fare;
	}

	
	
	/**
	 * @return id - Trip_id
	 */
	public String getTripid() {
		// TODO Auto-generated method stub
		return trip_id;
	}	


	public String getCompany() {
		// TODO Auto-generated method stub
		return company;
	}	

	public Date getDropoff_start_timestamp()
	{
		// TODO Auto-generated method stub
		return trip_start_timestamp;

	}	


	public Date getDropoff_end_timestamp() {
		// TODO Auto-generated method stub
		return trip_end_timestamp;

	}	


	public int getDropoff_community_area() {
		return dropoff_community_area;
	}

	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiid() {
		// TODO Auto-generated method stub
		return taxi_id;
	}	

	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return trip_seconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return trip_miles;
	}

	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return trip_total;
	}
	
	public int getPickup_community_area()
	{
		return pickup_community_area;
	}
	
	public double getFare()
	{
		return fare;
	}




	@Override
	public int compareTo(Servicio o) {
		// TODO Auto-generated method stub
		if(o.getDropoff_start_timestamp().equals(getDropoff_start_timestamp()))
		{
			return 0;
		}
		
		if(o.getDropoff_start_timestamp().before(getDropoff_start_timestamp()))
		{
			return 1;
			
		}
		
		return -1;
		
	}

	
}

