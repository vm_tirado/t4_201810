package controller;

import api.ITaxiTripsManager;
import model.logic.TaxiTripsManager;
import model.vo.Taxi;
import model.vo.Compania;
import model.vo.InfoTaxiRango;
import model.vo.RangoFechaHora;
import model.vo.Servicio;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	private static ITaxiTripsManager  manager = new TaxiTripsManager();
	
	/** To load the services of the taxi with taxiId */
	public static InfoTaxiRango[] loadServices( RangoFechaHora rango ) {
		// To define the dataset file's name 
		String serviceFile = "./data/taxi-trips-wrvz-psew-subset-medium.json";
		
	return	manager.loadServices( serviceFile, rango );
	}
		
	public static int [] servicesInInverseOrder() {
		return manager.servicesInInverseOrder();
	}
	
	public static Compania [] servicesPriority() {
		return manager.servicesPriority();
	}
}
